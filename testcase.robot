*** Settings ***
library    RequestsLibrary
library    OperatingSystem
Library	   JSONLibrary
Library    Collections

*** Variables ***
${endpoint}    https://inventory.example.com
${channel_id}    123456
${partner_id}    xxxxxx
${page}    1
${page_size}    100

${resp}    Set Variable    [
...    {
...        "qty": "0",
...        "sku": "SKU0001",
...        "updatedTime": "2021-08-05T10:20:12.053457Z"
...    },
...    {
...        "qty": "0",
...        "sku": "SKU0002",
...        "updatedTime": "2021-08-05T10:20:12.093643Z"
...    },
...   {
...        "qty": "0",
...        "sku": "SKU0003",
...        "updatedTime": "2021-08-05T10:20:12.121762Z"
...    },
...    {
...        "qty": "0",
...        "sku": "SKU0004",
...        "updatedTime": "2021-08-05T10:20:12.150911Z"
...    },
...    {
...        "qty": "0",
...        "sku": "SKU0005",
...        "updatedTime": "2021-08-05T10:20:12.180933Z"
...    }
...   ]

*** Test Cases ***
TC_001
    log many    ${endpoint}    /channel/${channel_id}/allocation/merchant/${partner_id}?page=${page}&page_size=${page_size}
    ${resp}    Get File    resp.txt
    log    ${resp}

TC_002
    ${resp}    Load JSON From File    resp.json
    ${length}    Get length    ${resp[0]}
    Should be Equal    ${length}    ${3}
    Dictionary Should Contain Key    ${resp[0]}    qty
    Dictionary Should Contain Key    ${resp[0]}    sku
    Dictionary Should Contain Key    ${resp[0]}    updatedTime